package alphacivsrc.domain;

import java.util.ArrayList;

public class BetaCivWinStrat implements WinStrategy {
	
	@Override
	public Player checkForWinner(int age, ArrayList<CityImpl> cities, Player playerInTurn) {
		for(CityImpl city:  cities) {
			if(!city.getOwner().equals(playerInTurn)) {
				return null;
			}
		}
		return playerInTurn;
	}

}
