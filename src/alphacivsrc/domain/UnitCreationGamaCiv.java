package alphacivsrc.domain;

public class UnitCreationGamaCiv implements UnitCreationFactory {

	@Override
	public UnitImpl createUnit(Player owner, String unitType) {
		if(unitType.equals(GameConstants.ARCHER)) {
			UnitImpl unit = new UnitImpl(owner, unitType, new ArcherActionImpl());
			return unit;
		}else if(unitType.equals(GameConstants.SETTLER)) {
			UnitImpl unit = new UnitImpl(owner, unitType, new SettlerAction());
			return unit;
		}
		UnitImpl unit = new UnitImpl(owner,unitType, new NoUnitActionImpl());
		return unit;
	}

}
