package alphacivsrc.domain;

public class UnitContext {
	private TileImpl tile;
	
	public UnitContext(TileImpl tile) {
		this.tile = tile;
	}

	public TileImpl getTile() {
		return tile;
	}

	public void setTile(TileImpl tile) {
		this.tile = tile;
	}
}
