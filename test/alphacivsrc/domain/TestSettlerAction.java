package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestSettlerAction {
	private SettlerAction settlerAction;

	@Before
	public void setUp(){
		settlerAction = new SettlerAction();
	}

	@Test
	public void testSettlerSettlesSettlement() {
		TileImpl tile = new TileImpl(new Position(8, 8), GameConstants.PLAINS);
		UnitImpl settler = new UnitImpl(Player.RED, GameConstants.SETTLER, new SettlerAction());
		tile.setUnit(settler);
		UnitContext unitContext = new UnitContext(tile);
		
		settlerAction.unitPreformsAction(unitContext);
		assertEquals(settler, settler);
		
		assertNotNull(tile.getCity());
	}

}
