package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestArcherAction {
	private ArcherActionImpl archerActionImpl;
	
	@Before
	public void setUp(){
		archerActionImpl = new ArcherActionImpl();
	}

	@Test
	public void testFortify() {
		TileImpl tile = new TileImpl(new Position(8, 8), GameConstants.PLAINS);
		UnitImpl archer = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		tile.setUnit(archer);
		UnitContext unitContext = new UnitContext(tile);
		
		int originalDefense = archer.getDefensiveStrength(); 
		archerActionImpl.unitPreformsAction(unitContext);
		assertEquals(originalDefense*2, archer.getDefensiveStrength());
		assertEquals(0, archer.getMoveCount());
	}
	
	@Test
	public void testUnfortify() {
		TileImpl tile = new TileImpl(new Position(8, 8), GameConstants.PLAINS);
		UnitImpl archer = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		tile.setUnit(archer);
		UnitContext unitContext = new UnitContext(tile);
		
		int originalDefense = archer.getDefensiveStrength(); 
		archerActionImpl.unitPreformsAction(unitContext);
		archerActionImpl.unitPreformsAction(unitContext);
		assertEquals(originalDefense, archer.getDefensiveStrength());
		assertEquals(1, archer.getMoveCount());
	}

}