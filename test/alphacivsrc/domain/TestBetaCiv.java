package alphacivsrc.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestBetaCiv {
	private GameImpl game;
	private TileImpl[][] boardArray;

	/** Fixture for betaciv testing. */
	@Before
	public void setUp() {
		game = new GameImpl(new ProgressiveAgingImpl(), new BetaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv());
		boardArray = ((GameImpl) game).getBoardArray();
	}
	
	//Integrated tests for aging
	@Test
	public void age100YearsPerRoundBefore100BC(){
		game.setAge(-4000);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age 100 years to -3900", -3900, game.getAge());
	}
	
	@Test
	public void ageTo1BCFrom100BC(){
		game.setAge(-100);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to -1 from -100", -1, game.getAge());
	}
	
	@Test
	public void ageTo1ADFrom1BC(){
		game.setAge(-1);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 1 from -1", 1, game.getAge());
	}
	
	@Test
	public void ageTo50ADFrom1AD(){
		game.setAge(1);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 50 from 1", 50, game.getAge());
	}
	
	@Test
	public void age50YearsAfter50AD(){
		game.setAge(50);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 100 from 50 ", 100, game.getAge());
	}
	
	@Test
	public void ageTo1775From1750(){
		game.setAge(1750);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 1775 from 1750", 1775, game.getAge());
	}
	
	@Test
	public void ageTo1905From1900(){
		game.setAge(1900);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 1905 from 1900", 1905, game.getAge());
	}
	
	@Test
	public void ageTo1971From1970(){
		game.setAge(1970);
		game.endOfTurn();
		game.endOfTurn();
		assertEquals("Should age to 1971 from 1970", 1971, game.getAge());
	}
	
	//Tests for conquering cities
	@Test
	public void redCanConquerBlueCity(){
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][2];
		tile.setUnit(unit);
		Position toPos = new Position(4,1);
		game.moveUnit(tile.getPosition(), toPos);
		assertNotNull(game.getUnitAt(toPos));
		City city = boardArray[4][1].getCity();
		assertEquals(Player.RED, city.getOwner());
	}
	
	@Test
	public void blueCanConquerRedCity(){
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[1][2];
		tile.setUnit(unit);
		Position toPos = new Position(1,1);
		game.moveUnit(tile.getPosition(), toPos);
		assertNotNull(game.getUnitAt(toPos));
		City city = boardArray[1][1].getCity();
		assertEquals(Player.BLUE, city.getOwner());
	}
	
	//Tests for winning through conquering cities
	@Test
	public void noRedCitiesBlueWins(){
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[1][2];
		tile.setUnit(unit);
		Position toPos = new Position(1,1);
		game.moveUnit(tile.getPosition(), toPos);
		assertNotNull(game.getUnitAt(toPos));
		City city = boardArray[1][1].getCity();
		assertEquals(Player.BLUE, city.getOwner());
		assertEquals(Player.BLUE, game.getWinner());
	}
	
	@Test
	public void noBlueCitiesRedWins(){
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][2];
		tile.setUnit(unit);
		Position toPos = new Position(4,1);
		game.moveUnit(tile.getPosition(), toPos);
		assertNotNull(game.getUnitAt(toPos));
		City city = boardArray[4][1].getCity();
		assertEquals(Player.RED, city.getOwner());
		assertEquals(Player.RED, game.getWinner());
	}
}
